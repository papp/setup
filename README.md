# setup.php

## What is Setup PHP 

Setup.php is an installer to deliver your PHP software.
You can make your software available on your server, which can download Setup.php during installation and install the latest version of your software.
Software updates are also possible.

You only need to provide the setup.php file to your customers. There is no zip file needed that your customers have to download.


## System compatible
**Database** 
* SQLite
* MySQL 5 / MariaDB

**PHP**
* PHP 8 or greater


## Create Setup.php
1. add to your project the three files in the root directory.
setup.php
setup.db
phpInstallMaker.php

2. start phpInstallMaker.php in your browser and enter your software details.
2.1. Under Database you can specify the required database with all tables. See the example that I have already prepared.
2.2. be sure to save
2.3. with Generate you can transfer all files of your project into setup.db.

The Wars, basically it's very simple, right? ;-)

3. you have to store the setup.db on your server. For software updates you only have to update this file on your server after you have rebuilt it with phpInstallMaker.
Also setup.php updates itself, you can deliver with your software. Great or?

4. now you only need to provide the setup.php file to your customers. I think you know best about this ;-)

## Update of Setup.php and phpInstallMaker.php
Simply download the latest stable version of these two php files and replace the old ones in your project.
Only then you create new update for your software this also current setup.php will take over.


## ToDos:
There are still some things I would like to add.
- Password protection for setup.php
- Update function of the projects
- Repair function of the projects
- Error removal and optimization