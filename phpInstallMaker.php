<?php
error_reporting(E_ERROR);
$D = $_REQUEST['D'];

#- Spalte CHMOD hinzufügen
$DIR =  __dir__.'/';
$dir = CFile::dir(['PATH' => $DIR]);

$SETUP = new setup();

$SQL_System = new SQLite3("setup.db",SQLITE3_OPEN_READWRITE);
$SQL_System->exec('
		PRAGMA busy_timeout = 5000;
		PRAGMA cache_size = -2000;
		PRAGMA synchronous = 1;
		PRAGMA foreign_keys = ON;
		PRAGMA temp_store = MEMORY;
		PRAGMA default_temp_store = MEMORY;
		PRAGMA read_uncommitted = true;
		PRAGMA journal_mode = wal;
		PRAGMA wal_autocheckpoint=1000;
');



switch($D['ACTION']) {
	case 'save': //Einstellungen nur Speichern
		$SETUP->set_setting($D);
		$SETUP->set_system_db($D);
		$SETUP->db_vacuum($D);
		unset($D['DB']);
		break;
	case 'creat': //Ertellern
		$SQL_System->exec('DELETE FROM wp_system_file');
		$SETUP->get_setting($D);
		$_setIgnore = explode("\n",str_replace("\r",'',$D['SETTING']['D']['FILE_IGNORE']['VALUE']));
		$SETUP->get_ignoreFile($D);
		$IGNORE = array_merge((array)$_setIgnore,['setup.db-wal','setup.db-shm','setup.db','phpInstallMaker.php']);
		foreach((array)$D['FILE_IGNORE']['D'] AS $kF => $F) {
			$IGNORE = array_merge($IGNORE,(array)$F['VALUE']);
		}
		
		dir_rec($DIR,$CFile,$SQL_System,$IGNORE);
		$SQL_System->exec("VACUUM");
		
		#Hinterlege Quelle in der setup.php
		if($D['SETTING']['D']['URL_SETUP_DB']['VALUE']) {
			$_File = file_get_contents("setup_default.php");
			
			$_File = str_replace('#<URL_DB>#','$URL_DB = "'.$D['SETTING']['D']['URL_SETUP_DB']['VALUE'].'";',$_File);
			file_put_contents('setup.php',$_File);
		}
		break;
}

$SETUP->get_setting($D);
$SETUP->get_system_db($D);
$SETUP->get_ignoreFile($D);
$SETUP->get_i18n($D);





function dir_rec($path,$CFile,$DB,$ignoreFile) {
    $dir = CFile::dir(['PATH' => $path]);
    $rel_patch = str_replace(__dir__.'/','',$path);
    if(!in_array(substr($rel_patch,0,-1),$ignoreFile) || $rel_patch == '') {
        foreach((array)$dir['DIR'] AS $kDir => $Dir ) {
            dir_rec($path.$Dir['NAME'].'/',$CFile,$DB,$ignoreFile);
        }
        foreach((array)$dir['FILE'] AS $kFile => $File) {
           
            if(!in_array($File['NAME'],$ignoreFile) && !in_array($rel_patch.$File['NAME'],$ignoreFile)) {
				$ID = md5($path.$File['NAME']);
                $MD = md5_file($path.$File['NAME']);
                $SIZE = filesize($path.$File['NAME']);
				$Stmt = $DB->prepare("INSERT INTO wp_system_file (id,file_hash, file_size, dir, filename,text) VALUES (:id, :file_hash, :file_size, :dir, :filename, :text)");
				$Stmt->bindValue(':id', $ID,SQLITE3_TEXT);
				$Stmt->bindValue(':file_hash', $MD,SQLITE3_TEXT);
				$Stmt->bindValue(':file_size', $SIZE,SQLITE3_TEXT);
				$Stmt->bindValue(':dir',$rel_patch,SQLITE3_TEXT);
				$Stmt->bindValue(':filename',$File['NAME'],SQLITE3_TEXT);
				$zipFile = gzcompress(file_get_contents($path.$File['NAME']),9);
				$Stmt->bindValue(':text',$zipFile,SQLITE3_BLOB);
				$Stmt->execute();
            }
            
        }
		
		if(!$dir['DIR'] && !$dir['FILE']) { #ist leerer ordner
			$ID = md5($path);
            $MD = '';
            $SIZE = 0;
			$Stmt = $DB->prepare("INSERT INTO wp_system_file (id,file_hash, file_size, dir, filename,text) VALUES (:id, :file_hash, :file_size, :dir, :filename, :text)");
			$Stmt->bindValue(':id', $ID,SQLITE3_TEXT);
			$Stmt->bindValue(':file_hash', $MD,SQLITE3_TEXT);
			$Stmt->bindValue(':file_size', $SIZE,SQLITE3_TEXT);
			$Stmt->bindValue(':dir',$rel_patch,SQLITE3_TEXT);
			$Stmt->bindValue(':filename','',SQLITE3_TEXT);
			$zipFile = '';
			$Stmt->bindValue(':text',$zipFile,SQLITE3_BLOB);
			$Stmt->execute();
		}
    }
}

class setup {
	function __construct() {
		
		if(!is_file('setup.db')) {
			$URL_DB = "https://gitlab.com/papp/setup/-/raw/main/setup.db";
			copy($URL_DB,'./setup.db');
		}
		if( is_file('setup.db')) {
			$this->SQL = new SQLite3("setup.db",SQLITE3_OPEN_READWRITE);
			$this->SQL->exec('
					PRAGMA busy_timeout = 5000;
					PRAGMA cache_size = -2000;
					PRAGMA synchronous = 1;
					PRAGMA foreign_keys = ON;
					PRAGMA temp_store = MEMORY;
					PRAGMA default_temp_store = MEMORY;
					PRAGMA read_uncommitted = true;
					PRAGMA journal_mode = wal;
					PRAGMA wal_autocheckpoint=1000;
					');
		}
		else {
			exit('setup.db not found!');
		}
	}
	
	function db_vacuum(&$D) {
		$this->SQL->exec('VACUUM;');
	}
	
	#nur .gitignore
	function get_ignoreFile(&$D) {
		if(is_file('.gitignore')) {
			$ig = CFile::read('.gitignore');
			$D['FILE_IGNORE']['D']['.gitignore']['VALUE'] = explode("\n",str_replace("\r",'',$ig));
		}
	}
	
	function set_setting(&$D) {
		foreach((array)$D['SETTING']['D'] AS $kS => $SETTING) {
			
				if($SETTING['ACTIVE'] != -2) {
					$IU_SETTING .= (($IU_SETTING)?',':'')."('{$kS}','{$SETTING['PARENT_ID']}','{$SETTING['LANGUAGE_ID']}'";
					#$IU_SETTING .= (isset($SETTING['LANGUAGE_ID']))? ",'".$this->SQL->escapeString($SETTING['LANGUAGE_ID'])."'":",''";
					$IU_SETTING .= (isset($SETTING['ACTIVE']))? ",'".$this->SQL->escapeString($SETTING['ACTIVE'])."'":",NULL";
					$IU_SETTING .= (isset($SETTING['VALUE']))? ",'".$this->SQL->escapeString($SETTING['VALUE'])."'":",NULL";
					$IU_SETTING .= (isset($SETTING['TYPE']))? ",'".$this->SQL->escapeString($SETTING['TYPE'])."'":",NULL";
					$IU_SETTING .= (isset($SETTING['VALUE_OPTION']))? ",'".$this->SQL->escapeString($SETTING['VALUE_OPTION'])."'":",NULL";
					
					$IU_SETTING .= ")";
				}
				else {
					$D_SETTING .= (($D_SETTING)?',':'')."'{$kS}'";
				}
			}

			$this->SQL->query("INSERT INTO wp_setting (id,parent_id,language_id,active,value,type,value_option) VALUES {$IU_SETTING} 
							ON CONFLICT(id,language_id) DO UPDATE SET
								id =			CASE WHEN excluded.id IS NOT NULL			AND ifnull(id,'') <> excluded.id						THEN excluded.id ELSE id END,
								parent_id =		CASE WHEN excluded.parent_id IS NOT NULL	AND ifnull(parent_id,'') <> excluded.parent_id			THEN excluded.parent_id ELSE parent_id END,
								language_id =	CASE WHEN excluded.language_id IS NOT NULL	AND ifnull(language_id,'') <> excluded.language_id		THEN excluded.language_id ELSE language_id END,
								active =		CASE WHEN excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active				THEN excluded.active ELSE active END,
								value =			CASE WHEN excluded.value IS NOT NULL		AND ifnull(value,'') <> excluded.value					THEN excluded.value ELSE value END,
								type =			CASE WHEN excluded.type IS NOT NULL			AND ifnull(type,'') <> excluded.type					THEN excluded.type ELSE type END,
								value_option =	CASE WHEN excluded.value_option IS NOT NULL	AND ifnull(value_option,'') <> excluded.value_option	THEN excluded.parent_id ELSE value_option END
							");
			if($D_SETTING) {
				$this->SQL->query("DELETE FROM wp_setting WHERE id IN ({$D_SETTING}) ");
			}
	}

	function get_setting(&$D=null) {
		$qry = $this->SQL->query("SELECT id AS ID,parent_id PARENT_ID,language_id LANGUAGE_ID,active ACTIVE,value VALUE,type TYPE,value_option VALUE_OPTION
							FROM wp_setting");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['SETTING']['D'][ $a['ID'] ] = $a;
		}
	}
	
	function get_i18n(&$D=null) {
		$qry = $this->SQL->query("SELECT id ID,language_id LANGUAGE_ID,value VALUE FROM wp_i18n");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['LANGUAGE']['D'][ $a['LANGUAGE_ID'] ]['I18N']['D'][ $a['ID'] ] = $a;
		}
	}
	
	function set_system_db(&$D) {
		foreach((array)$D['DB']['D'] AS $kD => $DB) {
			foreach((array)$DB['ID']['D'] AS $kV => $VER) {
				if($VER['ACTIVE'] != -2 ) {
					
					$IU_DB .= (($IU_DB)?',':'')."('{$kD}','{$kV}'";
					$IU_DB .= (isset($VER['DB_CODE']))? ",'".$this->SQL->escapeString($VER['DB_CODE'])."'":",NULL";
					
					$IU_DB .= ")";
				}
				else {
					$D_DB .= (($D_DB)?',':'')."'{$kD}{$kV}'";
				}
			}
		}
		if($IU_DB) {
			$this->SQL->query("INSERT INTO wp_system_db (db_id,id, db_code) VALUES {$IU_DB} 
						ON CONFLICT(id,db_id) DO UPDATE SET
							id =			CASE WHEN excluded.id IS NOT NULL			AND ifnull(id,'') <> excluded.id					THEN excluded.id ELSE id END,
							db_id =			CASE WHEN excluded.db_id IS NOT NULL		AND ifnull(db_id,'') <> excluded.db_id				THEN excluded.db_id ELSE db_id END,
							db_code =		CASE WHEN excluded.db_code IS NOT NULL		AND ifnull(db_code,'') <> excluded.db_code			THEN excluded.db_code ELSE db_code END
						");
		}
		if($D_DB) {
			$this->SQL->query("DELETE FROM wp_system_db WHERE db_id || id IN ({$D_DB}) ");
		}
	}
	
	function get_system_db(&$D=null) {
		$qry = $this->SQL->query("SELECT id ID, db_id DB_ID, db_code DB_CODE
							FROM wp_system_db
							ORDER BY db_id DESC
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['DB']['D'][ $a['DB_ID'] ]['ID']['D'][ $a['ID'] ] = $a;
		}
	}
}


class CFile {
	/**$D['FILE'] = 'etc/php5/'
	 */	
	static function dir($D) {
		if (is_dir($D['PATH'])) {
			if ($dh = opendir($D['PATH'])) {
				while (($file = readdir($dh)) !== false) {
					if($file != '.' && $file != '..')
					if(!is_file($D['PATH'] . $file) ) #filetype($D['PATH'] . $file) == 'dir')
					$D['DIR'][] = array(
						'NAME'	=>	$file,
					);
					else {
						$pi = pathinfo($file);
						$fi = stat($D['PATH'].$file);
						#Nur bei jpeg, && Tiff
						if(strtolower($pi['extension']) == 'jpg' || strtolower($pi['extension']) == 'jpeg' || $pi['extension'] == 'tiff') {
							$rd = exif_read_data($D['PATH'].$file, 0, false);
							
							if($rd['DateTimeOriginal'])
								$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeOriginal']);
							else if($rd['DateTimeDigitized'])
								$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeDigitized']);
							else if($rd['DateTime'])
								$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTime']);  
							
						}
						
						$D['FILE'][] = [
							'NAME'			=> $file,
							'FILENAME'		=> $pi['filename'],
							'EXTENSION'		=> $pi['extension'],
							'SIZE'			=> $fi['size'], #filesize($D['PATH'].$file),
							'CREATE_TIME'	=> date('YmdHis',$fi['ctime']),
							'EDIT_TIME'		=> date('YmdHis',$fi['mtime']),
							'RECORDING_TIME'=> $recording_time,#aufnahme Datum
						];
						
						
					};
					
				}
				closedir($dh);
			}
		}
		return $D;
	}
	
    static function mkdir($pfad, $D=null) {
		$D['CHMODE'] = (!$D['CHMODE'])?0777:$D['CHMODE'];
		$D['RECURSIVE'] = (!$D['RECURSIVE'])?true:$D['RECURSIVE'];
		$oldumask = umask(0);
		@mkdir($pfad, $D['CHMODE'], $D['RECURSIVE']);
		umask($oldumask);
	}
	
	static function read($url) {
		#if(!file_exists($url) || !is_readable($url))
		#	return FALSE;
		return file_get_contents($url);
	}
}

?>
<!doctype html>
<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
		
			<div class=" p-3">
				<form id="form" method="post">
					<input id="DACTION" type="hidden" name="D[ACTION]" value="save">
					
					<div class="row">
						<div class="col-3">		
							<nav style="position:fixed;">
							  <div class="nav flex-column nav-pills" id="nav-tab" >
								<a class="nav-link active"  data-toggle="pill" href="#nav-5">Programm Beschreibung</a>
								<a class="nav-link" data-toggle="pill" href="#nav-10">Einstellungen</a>
								<a class="nav-link" data-toggle="pill" href="#nav-20">Lizenz</a>
								<a class="nav-link" data-toggle="pill" href="#nav-30">Systemanforderungen</a>
								<a class="nav-link" data-toggle="pill" href="#nav-40">Datenbank</a>
							  </div>
							</nav>
						 </div>
						<div class="col-9">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-5">
									Programm Author<br>
									<input name="D[SETTING][D][APP_AUTHOR][VALUE]" class="form-control" style="width:300px;" value="<?php echo $D['SETTING']['D']['APP_AUTHOR']['VALUE']?>"><br>
									Programm Name<br>
									<input name="D[SETTING][D][APP_NAME][VALUE]" class="form-control" value="<?php echo $D['SETTING']['D']['APP_NAME']['VALUE']?>"><br>
									Programm beschreibung<br>
									<textarea name="D[SETTING][D][APP_DESCRIPTION][VALUE]" class="form-control" style="height:200px;"><?php echo $D['SETTING']['D']['APP_DESCRIPTION']['VALUE']?></textarea><br>
								</div>
								
								<div class="tab-pane fade show" id="nav-10">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">Setup Einstellungen</h5>
											Setup Sprachen (DE|EN|..)<br>
											<input name="D[SETTING][D][SETUP_LANGUAGE][VALUE]" class="form-control" value="<?php echo $D['SETTING']['D']['SETUP_LANGUAGE']['VALUE']?>"><br>
											Verfügbare Sprachen:<br>
											<?php foreach((array)$D['LANGUAGE']['D'] AS $kLan => $Lan) {?>
												<!--<input type="checkbox" id="cbLang" value="<?php echo $kLan?>"> <?php echo $Lan['I18N']['D']["language_{$kLan}"]['VALUE']?><br>-->
												<?php echo $kLan?> => <?php echo $Lan['I18N']['D']["language_{$kLan}"]['VALUE']?><br>
											<?php }?>
											<br>
											Ignore Files (Dateien und Ordner diese nicht ausgeliefert werde.)<br>
											<textarea name="D[SETTING][D][FILE_IGNORE][VALUE]" class="form-control" style="height:200px;"><?php echo implode("\n",(array)$D['SETTING']['D']['FILE_IGNORE']['VALUE'])?></textarea><br>
											<?php foreach((array)$D['FILE_IGNORE']['D'] AS $kF => $F) {?>
											.gitignore (Dateien und Ordner diese nicht ausgeliefert werde.)<br>
											<textarea readonly class="form-control" style="height:100px;"><?php echo implode("\n",(array)$D['FILE_IGNORE']['D'][$kF]['VALUE'])?></textarea><br>
											<?php }?>
											URL zur setup.db aus dieser heruntergeladen werden kann<br>
											<input name="D[SETTING][D][URL_SETUP_DB][VALUE]" class="form-control"  value="<?php echo $D['SETTING']['D']['URL_SETUP_DB']['VALUE']?>"><br>
											Inline CSS (Setup.php CSS Anpassungen)<br>
											<textarea name="D[SETTING][D][SETUP_INLINE_CSS][VALUE]" class="form-control" style="height:200px;font-family:monospace;font-size:initial;"><?php echo $D['SETTING']['D']['SETUP_INLINE_CSS']['VALUE']?></textarea>
										</div>
									</div>
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">Update Einstellungen</h5>
											Clean up Files, beim bereinigung Dateien und Ordner Ignorieren<br>
											<textarea name="D[SETTING][D][CLEANUP_FILE_IGNORE][VALUE]" class="form-control" style="height:200px;font-family:monospace;font-size:initial;"><?php echo $D['SETTING']['D']['CLEANUP_FILE_IGNORE']['VALUE']?></textarea>
										</div>
									</div>
								</div>
								
								<div class="tab-pane fade show" id="nav-20">
									Lizenz Text (optional)<br>
									<textarea name="D[SETTING][D][APP_LICENSE][VALUE]" class="form-control" style="height:200px;"><?php echo $D['SETTING']['D']['APP_LICENSE']['VALUE']?></textarea><br>
								</div>

								<div class="tab-pane fade" id="nav-30">
									Require Extensions (extension|operator|version)<a target="_blank" href="https://www.php.net/manual/de/function.version-compare.php">?</a><br>
									<textarea name="D[SETTING][D][APP_REQUIRE_EXTENSIONS][VALUE]" class="form-control" style="height:200px;"><?php echo $D['SETTING']['D']['APP_REQUIRE_EXTENSIONS']['VALUE']?></textarea>
									
									Software-Anforderungen Settings(extension|operator|version)<br>
									<textarea name="D[SETTING][D][APP_REQUIREMENTS][VALUE]" class="form-control" style="height:200px;"><?php echo $D['SETTING']['D']['APP_REQUIREMENTS']['VALUE']?></textarea>
								</div>
								<div class="tab-pane fade" id="nav-40">

									<?php $_DB = 0; while($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE']  != null || $_DB == 0 ){ ?>
									<div class="alert alert-secondary">
										
										Datenbank: <?php echo ($_DB+1)?><br>
										Datenbank Typ<br>
										<select class="form-control" name="D[SETTING][D][APP_DB_<?php echo $_DB?>_TYPE][VALUE]">
											<option value="" <?php echo ($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == '')?'selected':''; ?>>none</option>
											<option value="mysql" <?php echo ($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == 'mysql')?'selected':''; ?>>mysql (host,user,pass, db-name)</option>
											<option value="sqlite3" <?php echo ($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == 'sqlite3')?'selected':''; ?>>sqlite3</option>
										</select><br>
										
										Datenbank relativer Pfad (nur bei sqlite)<br>
										<input name="D[SETTING][D][APP_DB_<?php echo $_DB?>_PATH][VALUE]" class="form-control" value="<?php echo $D['SETTING']['D']["APP_DB_{$_DB}_PATH"]['VALUE']?>"><br>
										
										
										Datenbank Script Platzhalter definition (ID|Text)<br>
										<textarea name="D[SETTING][D][APP_DB_<?php echo $_DB?>_PLACEHOLDER][VALUE]" class="form-control" style="height:100px;"><?php echo $D['SETTING']['D']["APP_DB_{$_DB}_PLACEHOLDER"]['VALUE']?></textarea><br>
										Datenbank Script mit Platzhalter absetzen (Platzhalter "[ID]" )<br>
										<textarea name="D[SETTING][D][APP_DB_<?php echo $_DB?>_CODE][VALUE]" class="form-control" style="height:100px;"><?php echo $D['SETTING']['D']["APP_DB_{$_DB}_CODE"]['VALUE']?></textarea>
										<br>
										DB Code<br>
										<table class="table">
											<thead>
											<tr>
												<td style="width:10%">Active</td>
												<td style="width:10%">ID Version (>=1.00.000)</td>
												<td>SQL-CODE</td>
											</tr>
											</thead>
											<tbody>
											<?php $_nextVersion = ( ($D['DB']['D'][$_DB]['ID']['D'])?max((array)array_keys((array)$D['DB']['D'][$_DB]['ID']['D']))+1 : 100000)?>
											
											<tr>
												<td><input id="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $_nextVersion?>][ACTIVE]" name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $_nextVersion?>][ACTIVE]" style="width:50px;" value="-2"></td>
												<td><input name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $_nextVersion?>][ID]" class="form-control" value="<?php echo $_nextVersion?>"></td>
												<td><textarea onkeyup="document.getElementById('D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $_nextVersion?>][ACTIVE]').value = (this.value == '')?-2:1;" name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $_nextVersion?>][DB_CODE]" class="form-control"></textarea></td>
											</tr>
											<?php foreach((array)$D['DB']['D'][$_DB]['ID']['D'] AS $kDB => $vDB) {?>
											<tr <?php if( substr($kDB,-3) == '000') {?>style="background:#9de49f;"<?php }?>>
												<td>
													<input id="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $kDB?>][ACTIVE]" name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $kDB?>][ACTIVE]" style="width:50px;" value="<?php echo (!$vDB['DB_CODE'])? -2:1; ?>">
												</td>
												<td><input name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $kDB?>][ID]" class="form-control" value="<?php echo $vDB['ID']?>"></td>
												<td><textarea onkeyup="document.getElementById('D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $kDB?>][ACTIVE]').value = (this.value == '')?-2:1;" name="D[DB][D][<?php echo $_DB?>][ID][D][<?php echo $kDB?>][DB_CODE]" class="form-control"><?php echo $vDB['DB_CODE']?></textarea></td>
											</tr>
											<?php }?>
											</tbody>
										</table>
									
										
										
									</div>
									<?php $_DB++; }?>

								</div>
							</div>
						</div>
					</div>
				
					<div style="position:fixed; bottom:10px;">
						<button class="btn btn-primary">SAVE</button>
						<button class="btn btn-primary" type="button" onclick="$('#DACTION').val('creat');$( '#form' ).submit();">Generieren</button>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>