<?php
/** Setup 
* @link 
* @author 
* @copyright 2022
* @license https://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
* @version 1.0
*/
error_reporting(E_ERROR);

session_start();

$D = array_merge((array)$_SESSION['D'],(array)$_REQUEST['D']);



if(file_exists('setup.config.php')) {
	include_once ('setup.config.php'); //ToDo: Datenbank Zugang und Setup Key auslesen!
	$MODE = 'U';
} 
else {
	$MODE = 'I';
}


#AUTOCOPY-CONFIG-START
#<URL_DB>#
#AUTOCOPY-CONFIG-END

$SETUP = new setup($URL_DB);
$SETUP->get_setting($D);
$SETUP->get_version($D);
$SETUP->get_i18n($D);


$D['SETUP_LANGUAGE'] = $_SESSION['SETUP_LANGUAGE'] = $D['SETUP_LANGUAGE']??($_SESSION['SETUP_LANGUAGE']?? setup::getBrowserLang() ?? 'EN');
if( ! $D['LANGUAGE']['D'][ $D['SETUP_LANGUAGE'] ] ) {
	$D['SETUP_LANGUAGE'] = 'EN'; #Wenn die eingestellte Sprache nicht verfügbar ist, dann Standard Englisch 
}
#===============================================

switch($D['ACTION']) {
	case 'install': #Install or Update
		if(file_exists("phpInstallMaker.php")) { 
			exit ("Setup Maker is in the same folder. Installation is not carried out.");
		}
		if($MODE == 'I') { #Install
			#Dateien erzeugen
			$SETUP->extractfile();
			$SETUP->createDB($D);
			$SETUP->createConfig($D);
			echo "<script>next(1);</script>";
		}
		elseif($MODE == 'U') { #Update
			$SETUP->extractfile();
			#$SETUP->cleanUpFile();
			#ToDo: Settings müssen geladen werden aus setup.config.php
			#$SETUP->createDB($D); #DB Update
			echo "<script>next(1);</script>";	
		}
		break;
}

#=================================================
class setup {
	function __construct($URL_DB=null) {
		
		#Download DB
		if(is_file($URL_DB)) {
			copy($URL_DB,'./setup.db');
		}
		if( is_file('setup.db')) {
			$this->SQL = new SQLite3("setup.db", SQLITE3_OPEN_READONLY);
			$this->SQL->exec('
				PRAGMA busy_timeout = 5000;
				PRAGMA cache_size = -2000;
				PRAGMA synchronous = OFF;
				PRAGMA foreign_keys = ON;
				PRAGMA temp_store = MEMORY;
				PRAGMA default_temp_store = MEMORY;
				PRAGMA read_uncommitted = true;
				PRAGMA journal_mode = off;
				PRAGMA query_only = true;
			');
		}
		else {
			exit('setup.db not found!');
		}
	}
	
	function get_version(&$D=null) {
		$qry = $this->SQL->query("SELECT version VERSION
							FROM wp_version");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['SETUP']['VERSION'] = $a['VERSION'];
		}
		$qry = $this->SQL->query("SELECT max(id) ID
							FROM wp_system_db ORDER BY ID DESC LIMIT 1");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['SOFTWARE']['VERSION'] = $a['ID'];
		}
	}

	function get_setting(&$D=null) {
		$qry = $this->SQL->query("SELECT id AS ID,parent_id PARENT_ID,language_id LANGUAGE_ID,active ACTIVE,value VALUE,type TYPE,value_option VALUE_OPTION
							FROM wp_setting");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['SETTING']['D'][ $a['ID'] ] = $a;
		}
	}
	
	function get_i18n(&$D=null) {
		$qry = $this->SQL->query("SELECT id ID,language_id LANGUAGE_ID,value VALUE FROM wp_i18n");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['LANGUAGE']['D'][ $a['LANGUAGE_ID'] ]['I18N']['D'][ $a['ID'] ] = $a;
		}
	}
	
	function createConfig(&$D=null) {
		#ToDo:
		#$_File = '<?php $D=json_decode(\''.json_encode($_SESSION['D'])."',true);";
		$_File = '<?php $D=json_decode(\''.json_encode($_POST['D'])."',true);"; #Nur Einstellungen diese über Post übermittelt wurden 
		file_put_contents('setup.config.php',$_File);
	}
	
	#Entpake und ersetze Dateien aus der Datenbank
	function extractfile(&$D=null) {
		#Dateien erzeugen
		$qry = $this->SQL->query("SELECT id, dir, filename, text FROM wp_system_file");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			CFile::mkdir($a['dir']);	
			if($a['filename']) {
				$text = gzuncompress(($a['text']));
				file_put_contents($a['dir'].$a['filename'],$text);
			}
		}
	}

	#ToDo: veraltete Dateien diese nicht merh in der Datenbank exsistieren löschen. Außnahme, in der Configuration definieren
	function cleanUpFile(&$D=null) {
	/*
		$dir = CFile::dir([ 'PATH' => __dir__ ]);

		$D['SETTING']['D']['CLEANUP_FILE_IGNORE']['VALUE'];
		foreach((array)$D['DIR'] AS $kDir => $Dir) {
			$Dir['NAME'];
		}
		foreach((array)$D['FILE'] AS $kDir => $Dir) {
			$Dir['NAME'];
		}
	*/
		
	}
	
	#ersteleln und Updaten
	function createDB(&$D=null) {
		#DB
		$_DB = 0;
		do {
			if($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == 'sqlite3') { #SqLite3
				
				
				#Prüfe auf Platzhalter Pfad
				if(strpos($D['SETTING']['D']["APP_DB_{$_DB}_PATH"]['VALUE'],'*') === false) {
					$_DB_PATH[] = $D['SETTING']['D']["APP_DB_{$_DB}_PATH"]['VALUE'];
				}
				else { #Mit Platzhalter *
					
					$exaPfad = explode('*',$D['SETTING']['D']["APP_DB_{$_DB}_PATH"]['VALUE']);
					$d['PATH'] = $exaPfad[0];
					$d = CFile::dir($d);
					foreach((array)$d['DIR'] AS $kDir => $vDir) {
						$_DB_PATH[] = "{$d['PATH']}{$vDir['NAME']}{$exaPfad[1]}";
					}
				}
				
				foreach((array)$_DB_PATH AS $kPATH => $vPATH) {
					$_DBexists = is_file($vPATH);
					
					$SQL[$_DB] = new SQLite3( $vPATH );#DB ersteleln oder aufruffen
					$SQL[$_DB]->exec('PRAGMA busy_timeout = 5000;PRAGMA cache_size = -2000;PRAGMA synchronous = 1;PRAGMA foreign_keys = ON;PRAGMA temp_store = MEMORY;PRAGMA default_temp_store = MEMORY;PRAGMA read_uncommitted = true;PRAGMA journal_mode = wal;PRAGMA wal_autocheckpoint=1000;');			
						
					if( $_DBexists ) {
						$_DB_NAME = basename($vPAT);
						#DB Sicherung vor dem Update
						CFile::mkdir("data/BACKUP");
						copy($vPATH,"data/BACKUP/".date('YmdHis')."_{$_DB_NAME}");
					}
					else {
						#Version Tabelle erstellen
						$SQL[$_DB]->exec('CREATE TABLE "version" ("version" integer NOT NULL);');
						$SQL[$_DB]->exec('INSERT INTO "version" ("version") VALUES (0);');
						
						#Führe zusätzliche Installtion Script aus. Z.b. erstellen von Zugangsdaten
						$_SQL = $D['SETTING']['D']["APP_DB_{$_DB_CODE}"]['VALUE'];
						#ToDo: Platzhalter ersetzen
						$SQL[$_DB]->exec($_SQL);
					}
					
					#3. Datenbank Update
					$db_main_version = $SQL[$_DB]->querySingle('SELECT version FROM version');
					$qry = $this->SQL->query("SELECT id, db_code FROM wp_system_db WHERE db_id = '{$_DB}' ORDER BY id ASC");
					while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
						#main.db Update
						if($a['id'] > $db_main_version) {
							if($SQL[$_DB]->exec($a['db_code'])) {
								$SQL[$_DB]->exec("UPDATE version SET version = '{$a['id']}'");
							}
							else { #Fehler, Konnte Update nicht durchführen
								$D['ERROR'][] = "Datenbank: {$_DB} | Update: {$a['id']} konnte nicht durchgeführt werden";
								break;
							}
							#ToDo: PHP Script soll ebenfalls bei bedarf ausgeführt werden!
							$_File = "<?php /*update id: {$a['id']} */\n {$a['db_code']};?>";
							/*
							file_put_contents('setup.update.php',$_File);
							include('setup.update.php');
							unlink('setup.update.php');
							*/
							
						}
					}
					
					
					#4. INSTALL ----- #Führe spezifische SQL Befehle aus
					$_APP_DB_CODE[$_DB] = $D['SETTING']['D']["APP_DB_{$_DB}_CODE"]['VALUE'];
					foreach((array)$D['DB'][$_DB]['PLACEHOLDER'] AS $kPH => $vPH) {
						$_APP_DB_CODE[$_DB] = str_replace("[{$kPH}]","\"{$vPH}\"", $_APP_DB_CODE[$_DB]);
					}
					$SQL[$_DB]->exec($_APP_DB_CODE[$_DB]);
					#-------------
				}
				
				
			}
			elseif($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == 'mysql') { #MySql
				$SQL[$_DB] = new mysqli($D['DB'][$_DB]['HOST'], $D['DB'][$_DB]['USER'], $D['DB'][$_DB]['PASS'], $D['DB'][$_DB]['NAME'] );
				$result = $SQL[$_DB]->query("SHOW TABLES LIKE 'version'");
				if($result->num_rows == 0) {
					$SQL[$_DB]->query('CREATE TABLE "version" ("version" integer NOT NULL);');
					$SQL[$_DB]->query('INSERT INTO "version" ("version") VALUES (0);');
				}
				
				#ToDo: Sicherung der Datenbank?
				
				
				$result = $SQL[$_DB]->query('SELECT version FROM version');
				$row = $result->fetch_assoc();
				$db_main_version = $row['version'];
				$qry = $this->SQL->query("SELECT id, db_code FROM wp_system_db WHERE db_id = '{$_DB}' ORDER BY id ASC");
				while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
					if($a['id'] > $db_main_version) {
						if($SQL[$_DB]->query($a['db_code'])) {
							$SQL[$_DB]->query("UPDATE version SET version = '{$a['id']}'");
						}
						else { #Fehler, Konnte Update ncith durchführen
							$D['ERROR'][] = "Datenbank: {$_DB} | Update: {$a['id']} konnte nicht durchgeführt werden";
							break;
						}
					}
				}
				
				#DB Update
				
				#4. INSTALL ----- #Führe spezifische SQL Befehle aus
				$_APP_DB_CODE[$_DB] = $D['SETTING']['D']["APP_DB_{$_DB}_CODE"]['VALUE'];
				foreach((array)$D['DB'][$_DB]['PLACEHOLDER'] AS $kPH => $vPH) {
					$_APP_DB_CODE[$_DB] = str_replace("[{$kPH}]","\"{$vPH}\"", $_APP_DB_CODE[$_DB]);
				}
				$SQL[$_DB]->query($_APP_DB_CODE[$_DB]);
				#-------------
				
				#5. ToDo: Zusätzliche php Datei ausführen die während Update durchgeführt werden soll. Z.B: Um dateien aufzuräumen o.ä.
				#include('setup_install.php');
				#include('setup_update.php');
				#Achtung nach dem ausführen sollten diese Dateien wieder entfernt werden!
				
			}
			else {
				break;
			}
			$_DB++;
		} while($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE']);
	}
	
	#-1 = not Install; 0 = ist Install, Not Version; >0 have Version
	function getModulVersion($Modul) {
		switch ($Modul) {
			case 'php':
				return phpversion();
				break;
			case 'gd':
				if (extension_loaded('gd') && function_exists('gd_info')) {
					return  str_replace(['bundled (','compatible)'],[''], gd_info()["GD Version"]);
				}
				break;
			case 'curl':
				if (extension_loaded('curl') && function_exists('curl_version')) {
					return curl_version()['libz_version'];
				}
				break;
			case 'sqlite3':
				return SQLite3::version()['versionString'];
				break;
			default:
				$_iniExtensions = get_loaded_extensions();
				return (in_array($Modul,$_iniExtensions))?1:-1;
				break;
		}
		return -1;
	}

	static function getBrowserLang() {
		$lang_variable = explode(',',explode(';',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0]);
		return strtoupper($lang_variable[1]);
	}
}

class CFile {
	/**$D['FILE'] = 'etc/php5/'
	 */	
	static function dir($D)
	{
		if (is_dir($D['PATH']))
		{
			if ($dh = opendir($D['PATH']))
			{
				while (($file = readdir($dh)) !== false)
				{
					if($file != '.' && $file != '..')
					if(!is_file($D['PATH'] . $file) ) #filetype($D['PATH'] . $file) == 'dir')
					$D['DIR'][] = [
						'NAME'	=>	$file,
					];
					else
					{
						$pi = pathinfo($file);
						$fi = stat($D['PATH'].$file);
						#Nur bei jpeg, && Tiff
						if(strtolower($pi['extension']) == 'jpg' || strtolower($pi['extension']) == 'jpeg' || $pi['extension'] == 'tiff')
						{
							$rd = exif_read_data($D['PATH'].$file, 0, false);
							
							if($rd['DateTimeOriginal'])
							$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeOriginal']);
							else if($rd['DateTimeDigitized'])
								$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeDigitized']);
							else if($rd['DateTime'])
								$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTime']);  
							
						}
						
						$D['FILE'][] = [
							'NAME'			=> $file,
							'FILENAME'		=> $pi['filename'],
							'EXTENSION'		=> $pi['extension'],
							'SIZE'			=> $fi['size'], #filesize($D['PATH'].$file),
							'CREATE_TIME'	=> date('YmdHis',$fi['ctime']),
							'EDIT_TIME'		=> date('YmdHis',$fi['mtime']),
							'RECORDING_TIME'=> $recording_time,#aufnahme Datum
						];
						
						
					};
					
				}
				closedir($dh);
			}
		}
		return $D;
	}
    static function mkdir($pfad, $D=null)
	{
		$D['CHMODE'] = (!$D['CHMODE'])?0777:$D['CHMODE'];
		$D['RECURSIVE'] = (!$D['RECURSIVE'])?true:$D['RECURSIVE'];
		$oldumask = umask(0);
		@mkdir($pfad, $D['CHMODE'], $D['RECURSIVE']);
		umask($oldumask);
	}
	
	static function convertDataUnit( $size, $sourceUnit = 'B', $targetUnit = 'YB' ) {
		$units = [
			'bit' => 0,
			  'B' => 1,
			 'KB' => 2,
			 'MB' => 3,
			 'GB' => 4,
			 'TB' => 5,
			 'PB' => 6,
			 'EB' => 7,
			 'ZB' => 8,
			 'YB' => 9
		];
	 
		if( $units[$sourceUnit] <= $units[$targetUnit] ) {
			for( $i = $units[$sourceUnit]; $size >= 1024; $i++ ) {
				if( $i === 0 ) {
					$size /= 8;
				} else {
					$size /= 1024;
				}
			}
		} else {
			for( $i = $units[$sourceUnit]; $i > $units[$targetUnit]; $i-- ) {
				if( $i === 1 ) {
					$size *= 8;
				} else {
					$size *= 1024;
				}
			}
		}
		return round( $size, 2 ) . ' ' . array_keys($units)[$i];
	}
}

?>
<?php if(!$D['ACTION']) {?>
<!doctype html>
<html>
	<head>
		<link rel="icon" type="image/png" sizes="16x16" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAACvklEQVQ4jY1TS0wTARSclt1tWgr9Q6tVCAUMAgIWjCEEIRov4sWLCcgVTJAoxpNeOHAw0YhBiVlq8EAQCEeLB0JEiKJW2pIKVEqoVcuvtVBo6drdtesJgoEmzvHNZN7Le/NESILOW32GjDxDJwAEFlfb2h43rh6mI5IZKPSqhtOVZVcB4GOMsQF49F8GdBNNkrmGm5pMdT1JkQAAdYbmWu/DEeFHVPakvb2W368XAUB3x3ClUqFsiUajE+lqZX2xubA68UcQhUMhAIBSq4FYLBLmHXMT4Y3tl6mpsppIJPL0+t0rHwgA0Oq0HeYqcy0TY+opCQWfZwnaTAVOlBgBACu+AIKBiKjkbGkNG2drpDIpHO8degDnCbqJJjmW53iWh1QmxZLbg1Nn8iCRUntjZuUfhf4YC5dtEaaCfPAsDy7OcXQTTaZY7dZETW7FMCcmL8kV6QapTAytXoWf3tU12/jMoHfB75BIqCNqnUIe/80ikUiBZ9bjXJt1XWh51sLudbH2f3ZNvp4X1pe3hY0gIwzRo5Zdboges2wEGWHdvy1MjrgFa7/NtcuJk53xMAgQDtTEANDV2iXhOZZXajRY8QUAAAVleXVDljHLYM/o84JS02UAWPYFodKpwHEc39XaJQEAEd1Ek6nFOdbyKvNFgiLw3bOEk+acf5YIAHGGxbzdi6x8E3iWx/Q7++jOF2+duLmnmSMpgiQoAkyMgdGUDZdtEd6vfnBxDlycw7eFZczbvTCassHEGBAUAZIiyOaeZo4AgM3Q5r1Pbz7d2NmJvU1TpDUUVxRVJxKCaMHlBwQRVDoV9MZ0wTk1M7G1GR6Qy+Xnwlvh7r0k7kd7+zhxXB5r1ehUjYXlRWUAMDc96wwGf/X5o2kHopwUvfetd9zOkOB2hoQXD17dTqZL+o1ba5v9jilnOQCEl8MDyXR/Ad0LLZK8bLWGAAAAAElFTkSuQmCC" />
		
		<!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>-->
		<script src="//code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
		<style><?php echo $D['SETTING']['D']['SETUP_INLINE_CSS']['VALUE'];?></style>
		<script>
			var iActivePage = 0;
			function next(Pos) {
				var maxPage = $( ".tab" ).length;
				
				if(iActivePage+Pos > 0 && iActivePage+Pos <= maxPage) {
					$('.tab:nth-child(' + iActivePage + ')').hide(); //.toggle();
					$('.tab-title:nth-child(' + iActivePage + ')').removeClass('text-dark');
					$('.tab-title:nth-child(' + iActivePage + ')').addClass('text-muted');
				
					$('.tab:nth-child(' + (iActivePage + Pos) + ')').show(); //.toggle();
					$('.tab-title:nth-child(' + (iActivePage + Pos) + ')').removeClass('text-muted');
					$('.tab-title:nth-child(' + (iActivePage + Pos) + ')').addClass('text-dark');
					iActivePage = iActivePage+Pos;
				}

				if(iActivePage < 2) {
					$("#prevButton").hide();
				}
				else {
					$("#prevButton").show();
				}
				
				if(iActivePage < maxPage-2) {
					$("#nextButton").show();
					$("#setupButton").hide();
				}
				else {
					$("#nextButton").hide();
					$("#setupButton").show();
				}
			}
	
			var wp = wp || {};

			wp.ajax = function (D) {
				if (!isNaN(document.getElementById(D.div))) {
					$("body").append("<div id='" + D.div + "' style='display:none;'></div>");
				}

				if(D.INSERT == 'replace' || typeof D.INSERT == 'undefined') {
					$("#" + D.div).html('<style>.loader { border: 3px solid #f3f3f3;border-top: 3px solid #3498db;border-radius: 50%;width: 50px;height: 50px;animation: spin 2s linear infinite;position: absolute; left: 50%;top: 50%;  }@keyframes spin {0% { transform: rotate(0deg); }100% { transform: rotate(360deg); }}</style><div style="height:100%;width:100%;background: #222222;"><div><div class="loader"></div></div>');
				}
			   $.ajax({
					type: (D.method) ? D.method : 'POST',
					url: D.url+(((D.url).indexOf('?') > -1)?'&_=':'?_=')+Date.now(),
					data: D.data,
					cache: false,
					async: true 
				})
				.done(function (html) {
					switch (D.INSERT) {
						case 'append': //Anhängen am ende
							$("#" + D.div).append(html);
							break;
						case 'prepend': //Anhängen am Anfang
							$("#" + D.div).prepend(html);
							break;
						case 'replace': //Ersetzen
						default: //Neu Laden
							$("#" + D.div).html(html);
							break;
					}
					if (typeof D.done != 'undefined')
						D.done();
				});
			}
		</script>
		<meta charset="utf-8">
	</head>
	<body onload="next(1);">
		<div class="container" style="position:absolute; bottom: 0%; top:0%; left:0%; right:0%;">
			
			<div style="display: flex;
						align-items: center;
						flex-direction: column;
						justify-content: center;
						width: 100%;
						min-height: 100%;
						padding: 20px;">
	
			<div style="-webkit-border-radius: 10px 10px 10px 10px;
				border-radius: 5px;
				background: #eee;
				padding: 20px;
				width: 90%;
				
				position: relative;
				-webkit-box-shadow: 0 30px 60px 0 rgb(0 0 0 / 30%);
				box-shadow: 0 30px 60px 0 rgb(0 0 0 / 30%);
				text-align: center;
			">

			<form id="form" method="post" >
				
				
				<div class="row">
					<div class="col-12">
						<div style="float:left;">
							<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgo8c3ZnCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly93ZWIucmVzb3VyY2Uub3JnL2NjLyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB3aWR0aD0iNDgiCiAgIGhlaWdodD0iNDgiCiAgIGlkPSJzdmc3ODU0IgogICBzb2RpcG9kaTp2ZXJzaW9uPSIwLjMyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjQ1IgogICB2ZXJzaW9uPSIxLjAiCiAgIHNvZGlwb2RpOmRvY2Jhc2U9Ii9ob21lL2RvYmV5L1Byb2plY3RzL2dub21lLWljb24tdGhlbWUvc2NhbGFibGUvYXBwcyIKICAgc29kaXBvZGk6ZG9jbmFtZT0ic3lzdGVtLXNvZnR3YXJlLWluc3RhbGxlci5zdmciCiAgIGlua3NjYXBlOm91dHB1dF9leHRlbnNpb249Im9yZy5pbmtzY2FwZS5vdXRwdXQuc3ZnLmlua3NjYXBlIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzNzg1NiI+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyNTYiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmZmZmZmO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDYyNTgiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmZmY7c3RvcC1vcGFjaXR5OjA7IgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIGlkPSJzdG9wNjI2MCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyNDIiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZDdiNDdmO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDYyNDQiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmNGRiYjM7c3RvcC1vcGFjaXR5OjEiCiAgICAgICAgIG9mZnNldD0iMSIKICAgICAgICAgaWQ9InN0b3A2MjQ2IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjIzNCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmZmY7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNjIzNiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmZmZmZjtzdG9wLW9wYWNpdHk6MDsiCiAgICAgICAgIG9mZnNldD0iMSIKICAgICAgICAgaWQ9InN0b3A2MjM4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjIyMiI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wNjIyNCIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzAwMDAwMDtzdG9wLW9wYWNpdHk6MDsiCiAgICAgICAgIG9mZnNldD0iMSIKICAgICAgICAgaWQ9InN0b3A2MjI2IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjIxMCI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wNjIxMiIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZjllYWQyO3N0b3Atb3BhY2l0eToxOyIgLz4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3A2MjE0IgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNlNGE1NDY7c3RvcC1vcGFjaXR5OjE7IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjIwNCI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wNjIwNiIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojMTIwYzAyO3N0b3Atb3BhY2l0eToxOyIgLz4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3A2MjA4IgogICAgICAgICBvZmZzZXQ9IjEuMDAwMDAwMCIKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2I5N2ExYjtzdG9wLW9wYWNpdHk6MS4wMDAwMDAwOyIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYxOTAiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojYTA2NzBjO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDYxOTIiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNlNmM5OWQ7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIGlkPSJzdG9wNjE5NCIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQyMTAiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZWFiYTZmO3N0b3Atb3BhY2l0eToxLjAwMDAwMDA7IgogICAgICAgICBvZmZzZXQ9IjAuMDAwMDAwMCIKICAgICAgICAgaWQ9InN0b3A0MjEyIiAvPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojYjk3YTFiO3N0b3Atb3BhY2l0eToxLjAwMDAwMDA7IgogICAgICAgICBvZmZzZXQ9IjEuMDAwMDAwMCIKICAgICAgICAgaWQ9InN0b3A0MjE0IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjIwNCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDQ4NzkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMC40MTk5Njk0LC01LC0yLjk3Mjk3MDkpIgogICAgICAgeDE9IjI0Ljk5MDQ5OSIKICAgICAgIHkxPSI0OS40MjQwOTkiCiAgICAgICB4Mj0iMjMuNDUxNTcxIgogICAgICAgeTI9IjE0LjM4MjUxIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0idHJhbnNsYXRlKDAuNSwtMC4zNTM1NTMpIgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICB5Mj0iMTEuNjY5NjI4IgogICAgICAgeDI9IjE1Ljg4OTA3MiIKICAgICAgIHkxPSIyNC45MTk2MjgiCiAgICAgICB4MT0iMjUuOTg1OTI4IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDQxNSIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDQ0MDkiCiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIHkyPSIzNC4zMDc1IgogICAgICAgeDI9IjMyLjUxMSIKICAgICAgIHkxPSIxMS4xODg1IgogICAgICAgeDE9IjE0Ljk5NjYiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDEuMTkwNDc2LDAsMCwxLjE5MDQ3NiwtNC4zMDg5NDQsLTMuODMzMzIzKSIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MzUyNiIKICAgICAgIHhsaW5rOmhyZWY9IiNhaWdyZDEiCiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIHkyPSIxNC4yMDMzIgogICAgICAgeDI9IjM1LjM5MTIiCiAgICAgICB5MT0iMzIuNDE2NSIKICAgICAgIHgxPSIxMi4yNzQ0IgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgxLjE5MDQ3NiwwLDAsMS4xOTA0NzYsLTQuMzA4OTQ0LC0zLjgzMzMyNCkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDM1MjMiCiAgICAgICB4bGluazpocmVmPSIjYWlncmQyIgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDAuOTgwNzY4NSwwLDAsMC45ODA3Njg1LDAuMzgzNzEwNSwtMC44NTI1MzQ2KSIKICAgICAgIHkyPSI1NC42OTg0ODMiCiAgICAgICB4Mj0iNDguNzk4ODg1IgogICAgICAgeTE9IjMuNjEwMDE2MSIKICAgICAgIHgxPSIxMC41MDE3MjAiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDM1MTkiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ2MDM2IgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICB5Mj0iMTguMzY2NTc1IgogICAgICAgeDI9IjE3Ljc0MjcyOSIKICAgICAgIHkxPSIzMS40OTQ3MDciCiAgICAgICB4MT0iMjguNzAyODg1IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQzNTE1IgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjAyOCIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIgLz4KICAgIDxyYWRpYWxHcmFkaWVudAogICAgICAgcj0iMjIuNjI3NDE3IgogICAgICAgZnk9IjQxLjYzNjA0IgogICAgICAgZng9IjIzLjMzNDUyNCIKICAgICAgIGN5PSI0MS42MzYwNCIKICAgICAgIGN4PSIyMy4zMzQ1MjQiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KDEsMCwwLDAuMjUsMCwzMS4yMjcwMykiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGlkPSJyYWRpYWxHcmFkaWVudDM1MDciCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQyMzQxOSIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50MjM0MTkiCiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiPgogICAgICA8c3RvcAogICAgICAgICBpZD0ic3RvcDIzNDIxIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjE7IiAvPgogICAgICA8c3RvcAogICAgICAgICBpZD0ic3RvcDIzNDIzIgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwMDAwMDA7c3RvcC1vcGFjaXR5OjA7IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgeTI9IjM0LjMwNzUiCiAgICAgICB4Mj0iMzIuNTExIgogICAgICAgeTE9IjExLjE4ODUiCiAgICAgICB4MT0iMTQuOTk2NiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgaWQ9ImFpZ3JkMSI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wMzAzNCIKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I0VCRUJFQiIKICAgICAgICAgb2Zmc2V0PSIwIiAvPgogICAgICA8c3RvcAogICAgICAgICBpZD0ic3RvcDMwMzYiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNGRkZGRkYiCiAgICAgICAgIG9mZnNldD0iMC41IiAvPgogICAgICA8c3RvcAogICAgICAgICBpZD0ic3RvcDMwMzgiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNFQkVCRUIiCiAgICAgICAgIG9mZnNldD0iMSIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIHkyPSIxNC4yMDMzIgogICAgICAgeDI9IjM1LjM5MTIiCiAgICAgICB5MT0iMzIuNDE2NSIKICAgICAgIHgxPSIxMi4yNzQ0IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBpZD0iYWlncmQyIj4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3AzMDQzIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojRkJGQkZCIgogICAgICAgICBvZmZzZXQ9IjAiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wMzA0NSIKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I0I2QjZCNiIKICAgICAgICAgb2Zmc2V0PSIwLjUiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wMzA0NyIKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I0U0RTRFNCIKICAgICAgICAgb2Zmc2V0PSIxIiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjAzNiIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wNjAzOCIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmZmZmZmO3N0b3Atb3BhY2l0eToxOyIgLz4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3A2MDQwIgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmZmY7c3RvcC1vcGFjaXR5OjA7IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjAyOCIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wNjAzMCIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmZmZmZmO3N0b3Atb3BhY2l0eToxOyIgLz4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3A2MDMyIgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmZmY7c3RvcC1vcGFjaXR5OjA7IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NDQwOSIKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyI+CiAgICAgIDxzdG9wCiAgICAgICAgIGlkPSJzdG9wNDQxMSIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZmZmZmZmO3N0b3Atb3BhY2l0eToxOyIgLz4KICAgICAgPHN0b3AKICAgICAgICAgaWQ9InN0b3A0NDEzIgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZmZmZmY7c3RvcC1vcGFjaXR5OjA7IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjE5MCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYxOTgiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIHgxPSI2LjY3NDk5MDIiCiAgICAgICB5MT0iMTAuMTQwNzM1IgogICAgICAgeDI9IjgiCiAgICAgICB5Mj0iMTAuNjU0MDI5IiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ2MTkwIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjIwMCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgeDE9IjQxLjQ0ODIyMyIKICAgICAgIHkxPSI5LjM0NTIzOTYiCiAgICAgICB4Mj0iMzcuMzA4MDU2IgogICAgICAgeTI9IjEwLjc0MjQxNiIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjIxMCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyMTYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMS4zMjk5MDMsMCwtMTYuMTY0NDEpIgogICAgICAgeDE9IjM3LjMwODA1NiIKICAgICAgIHkxPSIyMC4yMzIxNzIiCiAgICAgICB4Mj0iNDMuNTg4Mzg3IgogICAgICAgeTI9IjIzLjQyMjE5NCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjIxMCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyMTgiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMS4zMjk5MDMsMCwtMTYuMTY0NDEpIgogICAgICAgeDE9IjgiCiAgICAgICB5MT0iMTYuNTI2OTYyIgogICAgICAgeDI9IjEuODI2NTcwNSIKICAgICAgIHkyPSIyMy40OTk3MTYiIC8+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDYyMjIiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ2MjI4IgogICAgICAgeDE9IjI0LjUiCiAgICAgICB5MT0iMTcuODEyNSIKICAgICAgIHgyPSIyNC41IgogICAgICAgeTI9IjI2LjE4NzUiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDIxMCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyMzIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMS4yMTMyNDQ4LC01LC0xMC4wODg1ODMpIgogICAgICAgeDE9IjI0Ljk5MDQ5OSIKICAgICAgIHkxPSI0OS40MjQwOTkiCiAgICAgICB4Mj0iMjMuNDUxNTcxIgogICAgICAgeTI9IjE0LjM4MjUxIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ2MjM0IgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjI0MCIKICAgICAgIHgxPSIyOC4wMjkyODciCiAgICAgICB5MT0iMzEuNzcyMDYiCiAgICAgICB4Mj0iMjcuMzc1IgogICAgICAgeTI9IjIwLjczMTI5NyIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ2MjQyIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50NjI0OCIKICAgICAgIHgxPSI2Ljk0NjE5MTgiCiAgICAgICB5MT0iOC4wNjY3Mzk5IgogICAgICAgeDI9IjM5LjA0NDMzOCIKICAgICAgIHkyPSI4LjA2NjczOTkiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NDIxMCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyNTIiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMS4zMjk5MDMsMCwtMTYuMTY0NDEpIgogICAgICAgeDE9IjE3LjMyNjU3MiIKICAgICAgIHkxPSIyNC4xNjM3MDIiCiAgICAgICB4Mj0iMTcuNzAxNTcyIgogICAgICAgeTI9IjI3Ljk0NTIwNCIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjE5MCIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyNTQiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIHgxPSIyNC44ODI5OSIKICAgICAgIHkxPSIxNC4yOTQ5ODciCiAgICAgICB4Mj0iMjUuMTQ4MTU1IgogICAgICAgeTI9IjEwLjk0MTk0MSIgLz4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeGxpbms6aHJlZj0iI2xpbmVhckdyYWRpZW50NjI1NiIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDYyNjIiCiAgICAgICB4MT0iMjQuOTM3NSIKICAgICAgIHkxPSIxOS4yMTg3NSIKICAgICAgIHgyPSIyNC45Mzc1IgogICAgICAgeTI9IjEzLjQ2MDI2NCIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiAvPgogICAgPGZpbHRlcgogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgeD0iLTAuMTExMTYwMzUiCiAgICAgICB3aWR0aD0iMS4yMjIzMjA3IgogICAgICAgeT0iLTAuNTA3MTY5MTIiCiAgICAgICBoZWlnaHQ9IjIuMDE0MzM4MiIKICAgICAgIGlkPSJmaWx0ZXI2MjkwIj4KICAgICAgPGZlR2F1c3NpYW5CbHVyCiAgICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgICAgc3RkRGV2aWF0aW9uPSIyLjAyODY3NjUiCiAgICAgICAgIGlkPSJmZUdhdXNzaWFuQmx1cjYyOTIiIC8+CiAgICA8L2ZpbHRlcj4KICA8L2RlZnM+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJiYXNlIgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iI2UwZTBlMCIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIGdyaWR0b2xlcmFuY2U9IjEwMDAwIgogICAgIGd1aWRldG9sZXJhbmNlPSIxMCIKICAgICBvYmplY3R0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp6b29tPSIxIgogICAgIGlua3NjYXBlOmN4PSI0My4wNzI5OTMiCiAgICAgaW5rc2NhcGU6Y3k9Ii04LjI3NTg4MSIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0icHgiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ibGF5ZXIxIgogICAgIHdpZHRoPSI0OHB4IgogICAgIGhlaWdodD0iNDhweCIKICAgICBpbmtzY2FwZTpzaG93cGFnZXNoYWRvdz0iZmFsc2UiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSI4ODAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iODI5IgogICAgIGlua3NjYXBlOndpbmRvdy14PSI3NjAiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9IjI4MCIKICAgICBzaG93Z3JpZD0iZmFsc2UiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhNzg1OSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6Y3JlYXRvcj4KICAgICAgICAgIDxjYzpBZ2VudD4KICAgICAgICAgICAgPGRjOnRpdGxlPkpha3ViIFN0ZWluZXI8L2RjOnRpdGxlPgogICAgICAgICAgPC9jYzpBZ2VudD4KICAgICAgICA8L2RjOmNyZWF0b3I+CiAgICAgICAgPGRjOnNvdXJjZT5odHRwOi8vamltbWFjLm11c2ljaGFsbC5jejwvZGM6c291cmNlPgogICAgICAgIDxjYzpsaWNlbnNlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9HUEwvMi4wLyIgLz4KICAgICAgICA8ZGM6dGl0bGU+U29mdHdhcmUgSW5zdGFsbGVyPC9kYzp0aXRsZT4KICAgICAgICA8ZGM6c3ViamVjdD4KICAgICAgICAgIDxyZGY6QmFnPgogICAgICAgICAgICA8cmRmOmxpPnNvZnR3YXJlPC9yZGY6bGk+CiAgICAgICAgICAgIDxyZGY6bGk+aW5zdGFsbDwvcmRmOmxpPgogICAgICAgICAgICA8cmRmOmxpPmluc3RhbGxlcjwvcmRmOmxpPgogICAgICAgICAgICA8cmRmOmxpPnNldHVwPC9yZGY6bGk+CiAgICAgICAgICA8L3JkZjpCYWc+CiAgICAgICAgPC9kYzpzdWJqZWN0PgogICAgICA8L2NjOldvcms+CiAgICAgIDxjYzpMaWNlbnNlCiAgICAgICAgIHJkZjphYm91dD0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvR1BMLzIuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vd2ViLnJlc291cmNlLm9yZy9jYy9SZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly93ZWIucmVzb3VyY2Uub3JnL2NjL0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly93ZWIucmVzb3VyY2Uub3JnL2NjL05vdGljZSIgLz4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3dlYi5yZXNvdXJjZS5vcmcvY2MvRGVyaXZhdGl2ZVdvcmtzIiAvPgogICAgICAgIDxjYzpyZXF1aXJlcwogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3dlYi5yZXNvdXJjZS5vcmcvY2MvU2hhcmVBbGlrZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly93ZWIucmVzb3VyY2Uub3JnL2NjL1NvdXJjZUNvZGUiIC8+CiAgICAgIDwvY2M6TGljZW5zZT4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxnCiAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIj4KICAgIDxyZWN0CiAgICAgICBzdHlsZT0ib3BhY2l0eTowLjU7Y29sb3I6IzAwMDAwMDtmaWxsOiMwMDAwMDA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7bWFya2VyOm5vbmU7bWFya2VyLXN0YXJ0Om5vbmU7bWFya2VyLW1pZDpub25lO21hcmtlci1lbmQ6bm9uZTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7dmlzaWJpbGl0eTp2aXNpYmxlO2Rpc3BsYXk6YmxvY2s7b3ZlcmZsb3c6dmlzaWJsZTtmaWx0ZXI6dXJsKCNmaWx0ZXI2MjkwKTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIgogICAgICAgaWQ9InJlY3Q2MjY0IgogICAgICAgd2lkdGg9IjM2LjUiCiAgICAgICBoZWlnaHQ9IjgiCiAgICAgICB4PSI1IgogICAgICAgeT0iMzEiCiAgICAgICByeD0iNCIKICAgICAgIHJ5PSI0IiAvPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7Y29sb3I6IzAwMDAwMDtmaWxsOnVybCgjbGluZWFyR3JhZGllbnQ2MjMyKTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6I2EwNjcwYztzdHJva2Utd2lkdGg6MS4wMDAwMDA4MztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTpibG9jaztvdmVyZmxvdzp2aXNpYmxlO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBpZD0icmVjdDQ4NzMiCiAgICAgICB3aWR0aD0iMzEiCiAgICAgICBoZWlnaHQ9IjI1Ljk5OTk5OCIKICAgICAgIHg9IjcuNSIKICAgICAgIHk9IjExLjUiCiAgICAgICByeD0iMS45MTQyMTM1IgogICAgICAgcnk9IjEuOTE0MjEzNSIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2NvbG9yOiMwMDAwMDA7ZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50NjIxNik7ZmlsbC1vcGFjaXR5OjEuMDtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDYyMDApO3N0cm9rZS13aWR0aDoxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO21hcmtlcjpub25lO21hcmtlci1zdGFydDpub25lO21hcmtlci1taWQ6bm9uZTttYXJrZXItZW5kOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3Zpc2liaWxpdHk6dmlzaWJsZTtkaXNwbGF5OmJsb2NrO292ZXJmbG93OnZpc2libGU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGQ9Ik0gMzguNSwxMS40ODAzOTMgTCA0NC41NTgwNTgsMTUuMjM0ODM2IEwgNDMuNjE2MTE3LDguNTI5ODcxIEwgMzguMzM4Mzg4LDQuNjcyMzM1MiBMIDM4LjUsMTEuNDgwMzkzIHoiCiAgICAgICBpZD0icGF0aDQ4ODEiCiAgICAgICBzb2RpcG9kaTpub2RldHlwZXM9ImNjY2NjIiAvPgogICAgPHJlY3QKICAgICAgIHJ5PSIwLjU0NDE5NDEiCiAgICAgICByeD0iMC41NDQxOTQxIgogICAgICAgeT0iNC41IgogICAgICAgeD0iNy41IgogICAgICAgaGVpZ2h0PSI5IgogICAgICAgd2lkdGg9IjMxIgogICAgICAgaWQ9InJlY3Q0ODc3IgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtjb2xvcjojMDAwMDAwO2ZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDQ4NzkpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTojYTA2NzBjO3N0cm9rZS13aWR0aDoxLjAwMDAwMDgzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO21hcmtlcjpub25lO21hcmtlci1zdGFydDpub25lO21hcmtlci1taWQ6bm9uZTttYXJrZXItZW5kOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3Zpc2liaWxpdHk6dmlzaWJsZTtkaXNwbGF5OmJsb2NrO292ZXJmbG93OnZpc2libGU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIgLz4KICAgIDxyZWN0CiAgICAgICBzdHlsZT0ib3BhY2l0eTowLjU1Mjk0MTE4O2NvbG9yOiMwMDAwMDA7ZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50NjIyOCk7ZmlsbC1vcGFjaXR5OjEuMDtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MS4wMDAwMDA4MztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTpibG9jaztvdmVyZmxvdzp2aXNpYmxlO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBpZD0icmVjdDYyMjAiCiAgICAgICB3aWR0aD0iMzAuMjUiCiAgICAgICBoZWlnaHQ9IjExLjYyNSIKICAgICAgIHg9IjcuNzUiCiAgICAgICB5PSIxNS4yNSIgLz4KICAgIDxwYXRoCiAgICAgICBzb2RpcG9kaTp0eXBlPSJpbmtzY2FwZTpvZmZzZXQiCiAgICAgICBpbmtzY2FwZTpyYWRpdXM9Ii0xLjA1NjUxNjgiCiAgICAgICBpbmtzY2FwZTpvcmlnaW5hbD0iTSA5LjQwNjI1IDExLjUgQyA4LjM0NTc3NTcgMTEuNSA3LjUgMTIuMzQ1Nzc2IDcuNSAxMy40MDYyNSBMIDcuNSAzNS41OTM3NSBDIDcuNSAzNi42NTQyMjQgOC4zNDU3NzYxIDM3LjQ5OTk5OCA5LjQwNjI1IDM3LjUgTCAzNi41OTM3NSAzNy41IEMgMzcuNjU0MjI0IDM3LjUgMzguNSAzNi42NTQyMjQgMzguNSAzNS41OTM3NSBMIDM4LjUgMTMuNDA2MjUgQyAzOC41IDEyLjM0NTc3NiAzNy42NTQyMjYgMTEuNSAzNi41OTM3NSAxMS41IEwgOS40MDYyNSAxMS41IHogIgogICAgICAgeGxpbms6aHJlZj0iI3JlY3Q0ODczIgogICAgICAgc3R5bGU9Im9wYWNpdHk6MC40NzY0NzA1OTtjb2xvcjojMDAwMDAwO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDYyNDApO3N0cm9rZS13aWR0aDoxLjAwMDAwMDgzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO21hcmtlcjpub25lO21hcmtlci1zdGFydDpub25lO21hcmtlci1taWQ6bm9uZTttYXJrZXItZW5kOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3Zpc2liaWxpdHk6dmlzaWJsZTtkaXNwbGF5OmJsb2NrO292ZXJmbG93OnZpc2libGU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGlkPSJwYXRoNjIzMCIKICAgICAgIGlua3NjYXBlOmhyZWY9IiNyZWN0NDg3MyIKICAgICAgIGQ9Ik0gOS40MDYyNSwxMi41NjI1IEMgOC45MDc1MjIyLDEyLjU2MjUgOC41NjI1LDEyLjkwNzUyMiA4LjU2MjUsMTMuNDA2MjUgTCA4LjU2MjUsMzUuNTkzNzUgQyA4LjU2MjUsMzYuMDkyNDc3IDguOTA3NTIwNCwzNi40Mzc0OTkgOS40MDYyNSwzNi40Mzc1IEwgMzYuNTkzNzUsMzYuNDM3NSBDIDM3LjA5MjQ3NywzNi40Mzc1IDM3LjQzNzUsMzYuMDkyNDc3IDM3LjQzNzUsMzUuNTkzNzUgTCAzNy40Mzc1LDEzLjQwNjI1IEMgMzcuNDM3NSwxMi45MDc1MjEgMzcuMDkyNDgsMTIuNTYyNSAzNi41OTM3NSwxMi41NjI1IEwgOS40MDYyNSwxMi41NjI1IHoiIC8+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtjb2xvcjojMDAwMDAwO2ZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDYyNTIpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTp1cmwoI2xpbmVhckdyYWRpZW50NjI1NCk7c3Ryb2tlLXdpZHRoOjEuMDAwMDAwODM7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7bWFya2VyOm5vbmU7bWFya2VyLXN0YXJ0Om5vbmU7bWFya2VyLW1pZDpub25lO21hcmtlci1lbmQ6bm9uZTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7dmlzaWJpbGl0eTp2aXNpYmxlO2Rpc3BsYXk6YmxvY2s7b3ZlcmZsb3c6dmlzaWJsZTtlbmFibGUtYmFja2dyb3VuZDphY2N1bXVsYXRlIgogICAgICAgZD0iTSA3LjU1ODA1ODMsMTEuNDQxOTQyIEwgNS41NTgwNTgzLDIwLjUgTCA0MC40Njk2NywyMC41IEwgMzguNTY2OTQyLDExLjQ3MjI3MiBMIDcuNTU4MDU4MywxMS40NDE5NDIgeiIKICAgICAgIGlkPSJwYXRoNDg4NSIKICAgICAgIHNvZGlwb2RpOm5vZGV0eXBlcz0iY2NjY2MiIC8+CiAgICA8cGF0aAogICAgICAgc29kaXBvZGk6dHlwZT0iaW5rc2NhcGU6b2Zmc2V0IgogICAgICAgaW5rc2NhcGU6cmFkaXVzPSItMC45OTkxMTc0MyIKICAgICAgIGlua3NjYXBlOm9yaWdpbmFsPSJNIDcuNTYyNSAxMS40Mzc1IEwgNS41NjI1IDIwLjUgTCA0MC40Njg3NSAyMC41IEwgMzguNTYyNSAxMS40Njg3NSBMIDcuNTYyNSAxMS40Mzc1IHogIgogICAgICAgeGxpbms6aHJlZj0iI3BhdGg0ODg1IgogICAgICAgc3R5bGU9Im9wYWNpdHk6MC4yNTg4MjM1Mztjb2xvcjojMDAwMDAwO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6dXJsKCNsaW5lYXJHcmFkaWVudDYyNjIpO3N0cm9rZS13aWR0aDoxLjAwMDAwMDgzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO21hcmtlcjpub25lO21hcmtlci1zdGFydDpub25lO21hcmtlci1taWQ6bm9uZTttYXJrZXItZW5kOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3Zpc2liaWxpdHk6dmlzaWJsZTtkaXNwbGF5OmJsb2NrO292ZXJmbG93OnZpc2libGU7ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgIGlkPSJwYXRoNjI1MCIKICAgICAgIGlua3NjYXBlOmhyZWY9IiNwYXRoNDg4NSIKICAgICAgIGQ9Ik0gOC4zNDM3NSwxMi40Mzc1IEwgNi44MTI1LDE5LjUgTCAzOS4yNSwxOS41IEwgMzcuNzUsMTIuNDY4NzUgTCA4LjM0Mzc1LDEyLjQzNzUgeiIgLz4KICAgIDxnCiAgICAgICBpbmtzY2FwZTpsYWJlbD0iTGF5ZXIgMSIKICAgICAgIGlkPSJnNTIxMiIKICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuNjU0NDMxNiwwLDAsMC42NzEwMjkxLC02LjE4NzI2MjZlLTIsMTUuNjU4NTc1KSI+CiAgICAgIDxwYXRoCiAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDEsMCwwLDEuMDY2MjkxLDAuOTE1NDgsLTQuMjE4NDI5KSIKICAgICAgICAgaW5rc2NhcGU6cl9jeT0idHJ1ZSIKICAgICAgICAgaW5rc2NhcGU6cl9jeD0idHJ1ZSIKICAgICAgICAgZD0iTSA0NS45NjE5NDEsNDEuNjM2MDQgQSAyMi42Mjc0MTcsNS42NTY4NTQyIDAgMSAxIDAuNzA3MTA3NTQsNDEuNjM2MDQgQSAyMi42Mjc0MTcsNS42NTY4NTQyIDAgMSAxIDQ1Ljk2MTk0MSw0MS42MzYwNCB6IgogICAgICAgICBzb2RpcG9kaTpyeT0iNS42NTY4NTQyIgogICAgICAgICBzb2RpcG9kaTpyeD0iMjIuNjI3NDE3IgogICAgICAgICBzb2RpcG9kaTpjeT0iNDEuNjM2MDQiCiAgICAgICAgIHNvZGlwb2RpOmN4PSIyMy4zMzQ1MjQiCiAgICAgICAgIGlkPSJwYXRoMjM0MTciCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNTU7Y29sb3I6IzAwMDAwMDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQzNTA3KTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZSIKICAgICAgICAgc29kaXBvZGk6dHlwZT0iYXJjIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MzUyNik7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NCIKICAgICAgICAgZD0iTSAyNC4yNjI0ODYsMi44MzMzNDM1IEMgMTIuNzE0ODY2LDIuODMzMzQzNSAzLjQyOTE1MTksMTIuMTE5MDU4IDMuNDI5MTUxOSwyMy42NjY2NzcgQyAzLjQyOTE1MTksMzUuMjE0Mjk3IDEyLjcxNDg2Niw0NC41MDAwMTIgMjQuMjYyNDg2LDQ0LjUwMDAxMiBDIDM1LjgxMDEwNiw0NC41MDAwMTIgNDUuMDk1ODIxLDM1LjIxNDI5NyA0NS4wOTU4MjEsMjMuNjY2Njc3IEMgNDUuMDk1ODIxLDEyLjExOTA1OCAzNS44MTAxMDYsMi44MzMzNDM1IDI0LjI2MjQ4NiwyLjgzMzM0MzUgTCAyNC4yNjI0ODYsMi44MzMzNDM1IHogTSAyNC4yNjI0ODYsMjguNjY2Njc4IEMgMjEuNTI0MzkxLDI4LjY2NjY3OCAxOS4yNjI0ODYsMjYuNDA0NzczIDE5LjI2MjQ4NiwyMy42NjY2NzcgQyAxOS4yNjI0ODYsMjAuOTI4NTgyIDIxLjUyNDM5MSwxOC42NjY2NzcgMjQuMjYyNDg2LDE4LjY2NjY3NyBDIDI3LjAwMDU4MSwxOC42NjY2NzcgMjkuMjYyNDg2LDIwLjkyODU4MiAyOS4yNjI0ODYsMjMuNjY2Njc3IEMgMjkuMjYyNDg2LDI2LjQwNDc3MyAyNy4wMDA1ODEsMjguNjY2Njc4IDI0LjI2MjQ4NiwyOC42NjY2NzggeiIKICAgICAgICAgaWQ9InBhdGgzMDQwIgogICAgICAgICBpbmtzY2FwZTpyX2N4PSJ0cnVlIgogICAgICAgICBpbmtzY2FwZTpyX2N5PSJ0cnVlIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50MzUyMyk7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiM4MDgwODA7c3Ryb2tlLXdpZHRoOjEuNTA5MDI3NjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGQ9Ik0gMjQuMjYyNDg2LDIuODMzMzQzMSBDIDEyLjcxNDg2NiwyLjgzMzM0MzEgMy40MjkxNTIsMTIuMTE5MDU4IDMuNDI5MTUyLDIzLjY2NjY3NyBDIDMuNDI5MTUyLDM1LjIxNDI5NyAxMi43MTQ4NjYsNDQuNTAwMDEyIDI0LjI2MjQ4Niw0NC41MDAwMTIgQyAzNS44MTAxMDYsNDQuNTAwMDEyIDQ1LjA5NTgyMSwzNS4yMTQyOTcgNDUuMDk1ODIxLDIzLjY2NjY3NyBDIDQ1LjA5NTgyMSwxMi4xMTkwNTggMzUuODEwMTA2LDIuODMzMzQzMSAyNC4yNjI0ODYsMi44MzMzNDMxIEwgMjQuMjYyNDg2LDIuODMzMzQzMSB6IE0gMjQuMjYyNDg2LDI4LjY2NjY3OCBDIDIxLjUyNDM5MSwyOC42NjY2NzggMTkuMjYyNDg2LDI2LjQwNDc3MyAxOS4yNjI0ODYsMjMuNjY2Njc3IEMgMTkuMjYyNDg2LDIwLjkyODU4MiAyMS41MjQzOTEsMTguNjY2Njc3IDI0LjI2MjQ4NiwxOC42NjY2NzcgQyAyNy4wMDA1ODEsMTguNjY2Njc3IDI5LjI2MjQ4NiwyMC45Mjg1ODIgMjkuMjYyNDg2LDIzLjY2NjY3NyBDIDI5LjI2MjQ4NiwyNi40MDQ3NzMgMjcuMDAwNTgxLDI4LjY2NjY3OCAyNC4yNjI0ODYsMjguNjY2Njc4IHoiCiAgICAgICAgIGlkPSJwYXRoMzA0OSIKICAgICAgICAgaW5rc2NhcGU6cl9jeD0idHJ1ZSIKICAgICAgICAgaW5rc2NhcGU6cl9jeT0idHJ1ZSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjc2Njc3NjY3NjIgogICAgICAgICBpZD0icGF0aDM1MzEiCiAgICAgICAgIGQ9Ik0gMjMuNDEwNTM4LDMuNDU4NzgyNSBDIDEyLjY0ODg0NywzLjg2NDY5MyA0LjExMDkyNzUsMTIuNjc2MTM1IDQuMTEwOTI3NSwyMy41NDEyMTcgQyA0LjExMDkyNzUsMjguNDY2NzIyIDUuODU4MDUxLDMyLjk0MzYwNyA4Ljc3Nzc2MDEsMzYuNDI3Njk3IEwgMTkuODk1MTEzLDI3LjAxNzQwMyBDIDE5LjE3Mzc3MiwyNi4xNzQwNjIgMTguODQ1ODc4LDI0Ljk3NDIzNSAxOC44NDU4NzgsMjMuNzkxMjE3IEMgMTguODQ1ODc4LDIwLjQwMDgyNyAyMS4xNTAxNjcsMTguMjIzODQyIDI0LjQxMzI1MywxOC4yMjM4NDIgQyAyNS44OTcxNzgsMTguMjIzODQyIDI3LjM4NDM0LDE4Ljg5NzI2OSAyOC4yNzE3MTksMTkuOTcwMTQgTCAzOS42MzkwNzIsMTAuNjg0ODQ2IEMgMzUuOTYzOTAxLDYuMjYwNjkxOSAzMC4zOTQ3OTcsMy40NTg3ODI1IDI0LjE2MzI1MywzLjQ1ODc4MjUgQyAyMy45MDI0OTIsMy40NTg3ODI1IDIzLjY2ODgxOSwzLjQ0OTA0MDYgMjMuNDEwNTM4LDMuNDU4NzgyNSB6IgogICAgICAgICBzdHlsZT0iZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50NDQxNSk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2Utb3BhY2l0eToxIiAvPgogICAgICA8cGF0aAogICAgICAgICBzdHlsZT0ib3BhY2l0eTowLjUxMDk4OTAyO2ZpbGw6I2VlZWVlYztmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2UtbWl0ZXJsaW1pdDo0IgogICAgICAgICBkPSJNIDI0LjI2MjQ4NiwxMy41NjI1MTIgQyAxOC42MTk5LDEzLjU2MjUxMiAxNC4xNTgzMiwxOC4xNTUzMTUgMTQuMTU4MzIsMjMuNjY2Njc3IEMgMTQuMTU4MzIsMjkuMzA5MjY1IDE4Ljc1MTEyNCwzMy43NzA4NDMgMjQuMjYyNDg2LDMzLjc3MDg0MyBDIDI5LjkwNTA3MiwzMy43NzA4NDMgMzQuMzY2NjUyLDI5LjE3ODA0MSAzNC4zNjY2NTIsMjMuNjY2Njc3IEMgMzQuMzY2NjUyLDE4LjAyNDA5MSAyOS43NzM4NDgsMTMuNTYyNTEyIDI0LjI2MjQ4NiwxMy41NjI1MTIgTCAyNC4yNjI0ODYsMTMuNTYyNTEyIHogTSAyNC4yNjI0ODYsMjkuMTc4MDQxIEMgMjEuMjQ0MzU5LDI5LjE3ODA0MSAxOC43NTExMjQsMjYuNjg0ODA2IDE4Ljc1MTEyNCwyMy42NjY2NzcgQyAxOC43NTExMjQsMjAuNjQ4NTUgMjEuMjQ0MzU5LDE4LjE1NTMxNSAyNC4yNjI0ODYsMTguMTU1MzE1IEMgMjcuMjgwNjEzLDE4LjE1NTMxNSAyOS43NzM4NDgsMjAuNjQ4NTUgMjkuNzczODQ4LDIzLjY2NjY3NyBDIDI5Ljc3Mzg0OCwyNi42ODQ4MDYgMjcuMjgwNjEzLDI5LjE3ODA0MSAyNC4yNjI0ODYsMjkuMTc4MDQxIHoiCiAgICAgICAgIGlkPSJwYXRoMzA1MSIKICAgICAgICAgaW5rc2NhcGU6cl9jeD0idHJ1ZSIKICAgICAgICAgaW5rc2NhcGU6cl9jeT0idHJ1ZSIgLz4KICAgICAgPHBhdGgKICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2MiCiAgICAgICAgIGlkPSJwYXRoNTI2NCIKICAgICAgICAgZD0iTSAyNC4yNjI0ODQsNC4yNDk3ODEzIEMgMTMuNDk5OTc3LDQuMjQ5NzgxMyA0Ljg0NTU5MDUsMTIuOTA0MTcgNC44NDU1OTA1LDIzLjY2NjY3NSBDIDQuODQ1NTkwNSwzNC40MjkxODIgMTMuNDk5OTc3LDQzLjA4MzU2OCAyNC4yNjI0ODQsNDMuMDgzNTY4IEMgMzUuMDI0OTksNDMuMDgzNTY4IDQzLjY3OTM3NiwzNC40MjkxODIgNDMuNjc5Mzc2LDIzLjY2NjY3NSBDIDQzLjY3OTM3NiwxMi45MDQxNyAzNS4wMjQ5OSw0LjI0OTc4MTMgMjQuMjYyNDg0LDQuMjQ5NzgxMyBMIDI0LjI2MjQ4NCw0LjI0OTc4MTMgeiIKICAgICAgICAgc3R5bGU9Im9wYWNpdHk6MC41NDY0NDgxMTtmaWxsOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQzNTE5KTtzdHJva2Utd2lkdGg6MS41MDkwMjc3MjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgIGlua3NjYXBlOnJfY3g9InRydWUiCiAgICAgICAgIGlua3NjYXBlOnJfY3k9InRydWUiIC8+CiAgICAgIDxwYXRoCiAgICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMuODY4ZS0zLC0xLjI0NDkzNCkiCiAgICAgICAgIGQ9Ik0gMzAuNDA1NTkxLDI0LjkzMDY0MSBBIDYuMDk4Nzk1OSw2LjA5ODc5NTkgMCAxIDEgMTguMjA3OTk5LDI0LjkzMDY0MSBBIDYuMDk4Nzk1OSw2LjA5ODc5NTkgMCAxIDEgMzAuNDA1NTkxLDI0LjkzMDY0MSB6IgogICAgICAgICBzb2RpcG9kaTpyeT0iNi4wOTg3OTU5IgogICAgICAgICBzb2RpcG9kaTpyeD0iNi4wOTg3OTU5IgogICAgICAgICBzb2RpcG9kaTpjeT0iMjQuOTMwNjQxIgogICAgICAgICBzb2RpcG9kaTpjeD0iMjQuMzA2Nzk1IgogICAgICAgICBpZD0icGF0aDYwMjYiCiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNjcyMTMxMTE7Y29sb3I6IzAwMDAwMDtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjAuMzE2Mzg0MTc7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQzNTE1KTtzdHJva2Utd2lkdGg6MS40MDQyMDg0MjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hvZmZzZXQ6MDtzdHJva2Utb3BhY2l0eToxO3Zpc2liaWxpdHk6dmlzaWJsZTtkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlIgogICAgICAgICBzb2RpcG9kaTp0eXBlPSJhcmMiCiAgICAgICAgIGlua3NjYXBlOnJfY3g9InRydWUiCiAgICAgICAgIGlua3NjYXBlOnJfY3k9InRydWUiIC8+CiAgICA8L2c+CiAgICA8cGF0aAogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtjb2xvcjojMDAwMDAwO2ZpbGw6dXJsKCNsaW5lYXJHcmFkaWVudDYyMTgpO2ZpbGwtb3BhY2l0eToxLjA7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOnVybCgjbGluZWFyR3JhZGllbnQ2MTk4KTtzdHJva2Utd2lkdGg6MS4wMDAwMDA4MztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTpibG9jaztvdmVyZmxvdzp2aXNpYmxlO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJNIDcuNTYyNSw0LjY4MzA1ODIgTCA3LjUsMTEuNSBMIDEuNjQ2NDQ2NiwxNi40OTk5OTkgTCAyLjUzMDMzMDEsOC42NDY0NDY2IEwgNy41NjI1LDQuNjgzMDU4MiB6IgogICAgICAgaWQ9InBhdGg0ODgzIgogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjYyIgLz4KICAgIDxwYXRoCiAgICAgICBzdHlsZT0ib3BhY2l0eToxO2NvbG9yOiMwMDAwMDA7ZmlsbDp1cmwoI2xpbmVhckdyYWRpZW50NjI0OCk7ZmlsbC1vcGFjaXR5OjEuMDtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MS4wMDAwMDA4MztzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjttYXJrZXI6bm9uZTttYXJrZXItc3RhcnQ6bm9uZTttYXJrZXItbWlkOm5vbmU7bWFya2VyLWVuZDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7ZGlzcGxheTpibG9jaztvdmVyZmxvdzp2aXNpYmxlO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICBkPSJNIDcuMDcxOTIyMyw2LjEwNDg1NDMgQyA2Ljk0MTQxNDQsNS42MTc3OTIzIDcuMjg4MDkzMSw0Ljk5ODc0MzYgNy45NTQ5NTA4LDQuOTk4NzQzNiBMIDM4LjEzMjU4Miw0Ljk1NTgwNTggQyAzOC41OTg2NzUsNC45NjUyMTM4IDM5LjAxMjQ4OSw1LjQzMDAzNjEgMzkuMDQ0MzM4LDUuOTcxNjc2MSBMIDM5LjAyNzI1MSwxMS4wNDk5OTUgTCAzOCwxMS4wMzY2MTIgTCAzOC4wMDY5ODksNi4wMTU1MzQ4IEwgOC4wODc1MzM4LDYuMDE1NTM0OCBMIDguMDQzMzM5NywxMC44NzY4OTQgTCA2Ljk0NjE5MTgsMTEuMTc3Njc0IEwgNy4wNzE5MjIzLDYuMTA0ODU0MyB6IgogICAgICAgaWQ9InBhdGg2MjAyIgogICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJjY2NjY2NjY2NjYyIgLz4KICA8L2c+Cjwvc3ZnPgo=">
							<b><?php echo $D['SETTING']['D']['APP_NAME']['VALUE'].' '.substr($D['SOFTWARE']['VERSION'],0,-5).'.'.substr($D['SOFTWARE']['VERSION'],-5,2).'.'.substr($D['SOFTWARE']['VERSION'],-3,3)?></b>
						</div>
						<div style="float:right;">
							<select name="D[SETUP_LANGUAGE]" onchange="window.open('?D[SETUP_LANGUAGE]='+this.value, '_self')">
								<?php $_LG = explode("|", $D['SETTING']['D']['SETUP_LANGUAGE']['VALUE']);
								foreach($_LG AS $kL => $vL) {?>
									<option value="<?php echo $vL;?>" <?php echo (($D['SETUP_LANGUAGE'] == $vL)?'selected':'')?>><?php echo $D['LANGUAGE']['D'][$vL]['I18N']['D']["language_{$vL}"]['VALUE']?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="col-3">	
						<?php /*ToDo: beim Update Modus, Punkte Lizenz, Datenbank, Einstellungen ausblenden. Punkt Installtion in Update unbennenen. Erkennung muss automatisch erfolgen.*/?>
						<ul style="text-align:left;margin:50%;margin-left:auto; margin-right:auto;">
							<?php if($D['SETTING']['D']['APP_DESCRIPTION']['VALUE']) {?>
							<li class="tab-title text-dark">Beschreibung</li>
							<?php }?>
							<?php if($D['SETTING']['D']['APP_REQUIRE_EXTENSIONS']['VALUE'] || $D['SETTING']['D']['APP_REQUIREMENTS']['VALUE']) {?>
							<li class="tab-title text-muted">Systemvoraussetzungen</li>
							<?php }?>
							<?php if($D['SETTING']['D']['APP_LICENSE']['VALUE']&& $MODE == 'I' ) {?>
							<li class="tab-title text-muted">Lizenz</li>
							<?php }?>
							<?php if($MODE == 'I') {?>
							<li class="tab-title text-muted">Instattionsort</li>
							<?php }?>
							<!--<li class="tab-title text-muted">Server Voraussetzungen</li>-->
							<?php if($D['SETTING']['D']['APP_DB_0_TYPE']['VALUE'] && $MODE == 'I') {?>
							<li class="tab-title text-muted">Datenbank</li>
							<?php }?>
							<?php if($D['SETTING']['D']['APP_DB_0_PLACEHOLDER']['VALUE'] && $MODE == 'I') {?>
							<li class="tab-title text-muted">Einstellungen</li>
							<?php }?>
							<li class="tab-title text-muted"><?php echo ($MODE == 'I')?"Installation":"Update"?></li>
							<li class="tab-title text-muted">Zusammenfassung</li>
						</ul>
	  
					</div>
					<div class="col-9">
						<div class="tab-main p-2" style="height:500px;overflow-y:auto;border-radius:5px;background:#fff">
							
							
							<?php if($D['SETTING']['D']['APP_DESCRIPTION']['VALUE']) {?>
							<div class="tab" style="display:none;">Beschreibung<br><?php echo $D['SETTING']['D']['APP_DESCRIPTION']['VALUE'];?></div>
							<?php }?>
							
							<?php if($D['SETTING']['D']['APP_REQUIRE_EXTENSIONS']['VALUE'] || $D['SETTING']['D']['APP_REQUIREMENTS']['VALUE']) {?>
							<div class="tab" style="display:none;">Systemvoraussetzungen<br>
							Modulerweiterungen<br>
							<table class="table">
								<thead>
									<tr>
										<th class='p-1'>Modul</th>
										<th class='p-1'>install Ver.</th>
										<th class='p-1'>Operator</th>
										<th class='p-1'>Need Ver.</th>
										<th class='p-1'>check</th>
									</tr>
								</thead>
							<?php $_ex = explode("\n", $D['SETTING']['D']['APP_REQUIRE_EXTENSIONS']['VALUE']);
								#$_iniExtensions = get_loaded_extensions();
								foreach((array)$_ex AS $kEX => $vEX ) {
									$_ex2 = explode("|", str_replace("\r",'', $vEX) );
									
									$_vers =  $SETUP->getModulVersion((string)$_ex2[0]);
									if($_ex2[1] && $_ex2[2]) {
										$_check = (($_vers && version_compare($_vers,$_ex2[2],trim($_ex2[1])))?'OK':'FAIL');
										$_textVersion = ($_vers)?$_vers : "not inst.";
										echo "<tr class='".(($_check == 'OK')?'table-success':'table-warning')."'>
												<td class='p-1'>{$_ex2[0]}</td>
												<td class='p-1'>{$_textVersion}</td>
												<td class='p-1'>{$_ex2[1]}</td>
												<td class='p-1'>{$_ex2[2]}</td>
												<td class='p-1'>{$_check}</td></tr>";
									}
								}
							?>
							</table>
							<br>
							PHP Konfiguration<br>
							<table class="table">
								<thead>
									<tr>
										<th class='p-1'>ini</th>
										<th class='p-1'>install Ver.</th>
										<th class='p-1'>Operator</th>
										<th class='p-1'>Need Ver.</th>
										<th class='p-1'>check</th>
									</tr>
								</thead>
							<?php $_ex = explode("\n", $D['SETTING']['D']['APP_REQUIREMENTS']['VALUE']);
								foreach((array)$_ex AS $kEX => $vEX ) {
									$_ex2 = explode("|", str_replace("\r",'', $vEX) );
									$_vers = ini_get($_ex2[0]);
									if($_ex2[1] && $_ex2[2]) {
										$_check = ((version_compare($_vers,$_ex2[2],trim($_ex2[1])))?'OK':'FAIL');
										echo "<tr class='".(($_check == 'OK')?'table-success':'table-warning')."'>
												<td class='p-1'>{$_ex2[0]}</td>
												<td class='p-1'>{$_vers}</td>
												<td class='p-1'>{$_ex2[1]}</td>
												<td class='p-1'>{$_ex2[2]}</td>
												<td class='p-1'>{$_check}</td></tr>";
									}
								}
							?>
							</table>
							</div>
							<?php }?>
							
							<?php if($D['SETTING']['D']['APP_LICENSE']['VALUE'] && $MODE == 'I') {?>
							<div class="tab" style="display:none;">Endbenutzer-Lizenzvertrag<br>
								<textarea style="width:100%;height:300px;"><?php echo $D['SETTING']['D']['APP_LICENSE']['VALUE'];?></textarea>
								<input id="cLizenz" type="checkbox" required> <label for="cLizenz">Ich akzeptiere die Bedienungen des Lizenzvertrags.</label>
							</div>
							<?php }?>
							
							<?php if($MODE == 'I') {?>
							<div class="tab" style="display:none;">
								Setup will die Software im folgenen Ordner installieren.<br>
								
								
								
								<div class="input-group mb-1">
								  <div class="input-group-prepend">
									<span class="input-group-text">Quelle:</span>
								  </div>
								   <label class="form-control"><?php echo $URL_DB?></label>
								</div>
								<div class="input-group mb-1">
								  <div class="input-group-prepend">
									<span class="input-group-text">Ziel:</span>
								  </div>
								   <label class="form-control"><?php echo __dir__?>/</label>
								</div>
								<!--
								<div class="input-group mb-1">
								  <div class="input-group-prepend">
									<span class="input-group-text">Benötigter Speicher:</span>
								  </div>
								   <label class="form-control"><?php echo CFile::convertDataUnit(1000)?></label>
								</div>
								-->
								<div class="input-group mb-1">
								  <div class="input-group-prepend">
									<span class="input-group-text">Verfügbare Speicher:</span>
								  </div>
								   <label class="form-control"><?php echo CFile::convertDataUnit(disk_free_space(__dir__))?></label>
								</div>
								<div class="input-group mb-3">
								  <div class="input-group-prepend">
									<span class="input-group-text">Gesamter Speicher:</span>
								  </div>
								   <label class="form-control"><?php echo CFile::convertDataUnit(disk_total_space(__dir__))?></label>
								</div>

							</div>
							<?php }?>
							
							<?php if($D['SETTING']['D']['APP_DB_0_TYPE']['VALUE'] && $MODE == 'I') {?>
							<div class="tab" style="display:none;">
							<?php $_DB = 0; while($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] ){ ?>
								<div>
									Datenbank: <?php echo ($_DB+1)?> (<?php echo $D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE']?>)<br>
									<?php if($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'] == 'mysql'){?>
									<div class="form-group">
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<label class="input-group-text">host:</label>
											</div>
											<input class="form-control" name='D[DB][<?php echo $_DB?>][HOST]' value="localhost" required>
										</div>
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<label class="input-group-text">User:</label>
											</div>
											<input class="form-control" name='D[DB][<?php echo $_DB?>][USER]' required>
										</div>
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<label class="input-group-text">Passwort:</label>
											</div>
											<input class="form-control" name='D[DB][<?php echo $_DB?>][PASS]' required>
										</div>
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<label class="input-group-text">DB-Name:</label>
											</div>
											<input class="form-control" name='D[DB][<?php echo $_DB?>][NAME]' required>
										</div>
									</div>
									<?php }?>
								</div>
								<?php $_DB++; }?>
								
							</div>
							<?php }?>
							
							<?php if($D['SETTING']['D']['APP_DB_0_PLACEHOLDER']['VALUE'] && $MODE == 'I') {?>
							<div class="tab" style="display:none;">
								<?php $_DB = 0; while($D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE']  != null ){ ?>
									<?php if($D['SETTING']['D']["APP_DB_{$_DB}_PLACEHOLDER"]['VALUE']) {?>
									<div>für Datenbank: <?php echo ($_DB+1)?> (<?php echo $D['SETTING']['D']["APP_DB_{$_DB}_TYPE"]['VALUE'];?>)<br>
										<?php $PH = explode("\r", $D['SETTING']['D']["APP_DB_{$_DB}_PLACEHOLDER"]['VALUE']);
											foreach((array)$PH AS $kPH => $vPH) {
												$Input = explode("|", trim($vPH));?>
												<div class="input-group mb-1">
													<div class="input-group-prepend">
														<label class="input-group-text"><?php echo $Input[1]?></label>
													</div>
														<input class='form-control' name='D[DB][<?php echo $_DB?>][PLACEHOLDER][<?php echo $Input[0]?>]' value='' required>
												</div>
										<?php }?>
									</div>
									<?php }?>
								<?php $_DB++; }?>
							</div>
							<?php }?>
							
							<div class="tab" id="tabInstall" style="display:none;"><?php echo ($MODE == 'I')?'Installation':'Update'?></div>
							<div class="tab" style="display:none;">Zusammenfassung<br>
								Installtion durchgeführt!
							</div>
							
						</div>
					
					</div>
					<div class="col-12 pt-2">
						<div style="float:left;font-size:10px;font-variant:all-petite-caps;padding-top:15px;">
							Setup: <?php echo substr($D['SETUP']['VERSION'],0,-5).'.'.substr($D['SETUP']['VERSION'],-5,2).'.'.substr($D['SETUP']['VERSION'],-3,3)?>
						</div>
						<div id="Navigation" style="float:right;">
							<button id="prevButton" type="button" class="btn btn-secondary btn-sm" onclick="next(-1);">Zurück</button>
							<button id="nextButton" type="button" class="btn btn-primary btn-sm" onclick="next(1);">Weiter</button>
							<input type="hidden" name="D[ACTION]" value="install">
							<button id="setupButton" class="btn btn-primary btn-sm" type="button" onclick="next(1);wp.ajax({'div' : 'tabInstall', url : '?D[ACTION]=install', data : $('form').serialize()});$('#Navigation').hide();"><?php if($MODE == 'I') {?>Installieren<?php }else {?>Update<?php }?></button>
						</div>
					</div>
				</div>
				
		
			</form>

			
				</div>		
			</div>
			
			
			
			
		</div>
	</body>
<?php }?>
